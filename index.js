const express = require("express");
let swig = require("swig");
const bodyParser = require("body-parser");
const fs = require("fs");
const multer = require("multer");

const app = express();
app.listen(3001, () => {
  console.log("Listening to server 3001");
});
swig = new swig.Swig();
app.engine("html", swig.renderFile);
app.set("view engine", "html");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static("public"));
app.set("views", "public/Plugins/plupload/examples/jquery");

app.get("/", (req, res) => {
  res.render("queue_widget");
});

const uploadPath = `public/Plugins/plupload/examples/uploads`;
checkDirectorySync(uploadPath);

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});

const upload = multer({ storage: storage });

app.post("/upload", upload.single("file"), (req, res) => {
  res.send();
});

function checkDirectorySync(directory) {
  try {
    fs.statSync(directory);
  } catch (e) {
    fs.mkdirSync(directory);
  }
}
